package main

import (
	"reflect"
	"testing"
)

func TestSave_SaveAndGet_Success(t *testing.T) {
	cacheService := CacheService{Path: "/var/tmp"}
	snackableFileWithMetadata := SnackableFileWithMetadata{FileId: "aa-bb", ProcessingStatus: "FINISHED"}
	cacheService.save("test-cache", snackableFileWithMetadata)

	if cacheService.exists("test-cache") == false {
		t.Error("Cache should exist")
	}
	if cacheService.exists("test-no-cache") == true {
		t.Error("Cache should not exist")
	}

	cachedFileWithMetadata := SnackableFileWithMetadata{}
	err := cacheService.get("test-cache", &cachedFileWithMetadata)

	if err != nil {
		t.Error("No errors should be provided")
	}
	if !reflect.DeepEqual(cachedFileWithMetadata, snackableFileWithMetadata) {
		t.Errorf("got %q, expected %q", cachedFileWithMetadata, snackableFileWithMetadata)
	}
}
