package main

import (
	"encoding/json"
	"log"
	"os"
)

type ICacheService interface {
	save(name string, data interface{})
	get(name string, data interface{}) error
	exists(name string) bool
}

type CacheService struct {
	Path string
}

func (cacheService CacheService) save(name string, data interface{}) {
	content, err := json.Marshal(data)
	if err != nil {
		log.Println(err)
	}
	if err := os.WriteFile(cacheService.Path+"/"+name, content, 0644); err != nil {
		log.Println(err)
	}
}

func (cacheService CacheService) get(name string, data interface{}) error {
	content, err := os.ReadFile(cacheService.Path + "/" + name)
	if err != nil {
		return err
	}

	err = json.Unmarshal(content, data)
	if err != nil {
		return err
	}

	return nil
}

func (cacheService CacheService) exists(name string) bool {
	_, err := os.Stat(cacheService.Path + "/" + name)
	return !os.IsNotExist(err)
}
