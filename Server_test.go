package main

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetFileWithMetadata_Success(t *testing.T) {
	request := mockGetRequest("/", map[string]string{"fileId": "aa-bb"})
	response := httptest.NewRecorder()

	server := SnackableFileServer{
		fileService:  MockFileService{},
		cacheService: MockCacheService{},
	}
	server.fileWithMetadata(response, request)

	if response.Code != 200 {
		t.Error("response code is not 200")
	}

	serverResponse := response.Body.String()
	expectedResponse := "{\"fileId\":\"aa-bb\",\"processingStatus\":\"FINISHED\"}"
	if serverResponse != expectedResponse {
		t.Errorf("got %q, expected %q", serverResponse, expectedResponse)
	}
}

func TestGetFileWithMetadata_Failed_EndpointNotFound(t *testing.T) {
	request := mockGetRequest("/wrong", map[string]string{})
	response := httptest.NewRecorder()

	server := SnackableFileServer{}
	server.fileWithMetadata(response, request)

	if response.Code != 404 {
		t.Error("response code is not 404")
	}

	serverResponse := response.Body.String()
	expectedResponse := "{\"statusCode\":404,\"message\":\"Endpoint not found\"}"
	if serverResponse != expectedResponse {
		t.Errorf("got %q, expected %q", serverResponse, expectedResponse)
	}
}

func TestGetFileWithMetadata_PostRequest_Failed_MethodNotAllowed(t *testing.T) {
	request := mockPostRequest("/", map[string]string{})
	response := httptest.NewRecorder()

	server := SnackableFileServer{}
	server.fileWithMetadata(response, request)

	if response.Code != 405 {
		t.Error("response code is not 405")
	}

	serverResponse := response.Body.String()
	expectedResponse := "{\"statusCode\":405,\"message\":\"Method not allowed\"}"
	if serverResponse != expectedResponse {
		t.Errorf("got %q, expected %q", serverResponse, expectedResponse)
	}
}

func TestGetFileDetails_Failed_FileIdNotProvided(t *testing.T) {
	request := mockGetRequest("/", map[string]string{})
	response := httptest.NewRecorder()

	server := SnackableFileServer{}
	server.fileWithMetadata(response, request)

	if response.Code != 400 {
		t.Error("response code is not 400")
	}

	serverResponse := response.Body.String()
	expectedResponse := "{\"statusCode\":400,\"message\":\"Bad request\"}"
	if serverResponse != expectedResponse {
		t.Errorf("got %q, expected %q", serverResponse, expectedResponse)
	}
}

func TestGetFileDetails_Failed_FailedToLoadFileInfo(t *testing.T) {
	request := mockGetRequest("/", map[string]string{"fileId": "aa-cc"})
	response := httptest.NewRecorder()

	server := SnackableFileServer{
		fileService:  MockFileService{},
		cacheService: MockCacheService{},
	}
	server.fileWithMetadata(response, request)

	if response.Code != 500 {
		t.Error("response code is not 500")
	}

	serverResponse := response.Body.String()
	expectedResponse := "{\"statusCode\":500,\"message\":\"File data loading failed\"}"
	if serverResponse != expectedResponse {
		t.Errorf("got %q, expected %q", serverResponse, expectedResponse)
	}
}

func mockGetRequest(url string, params map[string]string) *http.Request {
	request, _ := http.NewRequest(http.MethodGet, url, nil)
	q := request.URL.Query()
	for key, value := range params {
		q.Add(key, value)
	}

	request.URL.RawQuery = q.Encode()
	return request
}

func mockPostRequest(url string, params map[string]string) *http.Request {
	request, _ := http.NewRequest(http.MethodPost, url, nil)
	q := request.URL.Query()
	for key, value := range params {
		q.Add(key, value)
	}

	request.URL.RawQuery = q.Encode()
	return request
}

type MockFileService struct {
	IFileService
}

func (fileService MockFileService) findSnackableFile(fileId string) (SnackableFile, error) {
	if fileId != "aa-bb" {
		return SnackableFile{FileId: fileId, ProcessingStatus: "PROCESSING"}, nil
	}

	return SnackableFile{FileId: "aa-bb", ProcessingStatus: "FINISHED"}, nil
}

func (fileService MockFileService) loadFileWithMetadata(snackableFile SnackableFile) (SnackableFileWithMetadata, error) {
	if snackableFile.FileId != "aa-bb" {
		return SnackableFileWithMetadata{}, errors.New("Wrong file id")
	}

	return SnackableFileWithMetadata{FileId: snackableFile.FileId, ProcessingStatus: "FINISHED"}, nil
}

type MockCacheService struct {
	ICacheService
}

func (cacheService MockCacheService) save(name string, data interface{}) {}
func (cacheService MockCacheService) get(name string, data interface{}) error {
	return errors.New("file not found")
}
