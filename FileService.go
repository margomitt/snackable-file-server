package main

import (
	"errors"
	"log"
)

type SnackableFileWithMetadata struct {
	FileId           string                 `json:"fileId"`
	ProcessingStatus string                 `json:"processingStatus"`
	FileName         string                 `json:"fileName,omitempty"`
	Mp3Path          string                 `json:"mp3Path,omitempty"`
	OriginalFilepath string                 `json:"originalFilepath,omitempty"`
	SeriesTitle      string                 `json:"seriesTitle,omitempty"`
	Segments         []SnackableFileSegment `json:"segments,omitempty"`
}

type IFileService interface {
	loadFileWithMetadata(snackableFile SnackableFile) (SnackableFileWithMetadata, error)
	findSnackableFile(fileId string) (SnackableFile, error)
	findAllProcessedFiles() []SnackableFile
}

type FileService struct {
	IFileService
	fileClient IFileClient
}

func (fileService FileService) loadFileWithMetadata(snackableFile SnackableFile) (SnackableFileWithMetadata, error) {
	if snackableFile.ProcessingStatus != "FINISHED" {
		return SnackableFileWithMetadata{}, errors.New("File is not in FINISHED status")
	}

	fileDetails, err := fileService.fileClient.loadFileDetails(snackableFile.FileId)
	if err != nil {
		return SnackableFileWithMetadata{}, err //TODO maybe continue without file details
	}

	fileSegments, err := fileService.fileClient.loadFileSegments(snackableFile.FileId)
	if err != nil {
		return SnackableFileWithMetadata{}, err //TODO maybe continue without file segments data
	}

	return SnackableFileWithMetadata{
		FileId:           snackableFile.FileId,
		ProcessingStatus: snackableFile.ProcessingStatus,
		FileName:         fileDetails.FileName,
		Mp3Path:          fileDetails.Mp3Path,
		OriginalFilepath: fileDetails.OriginalFilepath,
		SeriesTitle:      fileDetails.SeriesTitle,
		Segments:         fileSegments,
	}, nil
}

func (fileService FileService) findSnackableFile(fileId string) (SnackableFile, error) {
	limit := 5
	offset := 0
	for true {
		loadedFiles, err := fileService.loadNextFiles(limit, offset)
		if err != nil {
			return SnackableFile{}, err
		}

		for _, file := range loadedFiles {
			if file.FileId == fileId {
				return file, nil
			}
		}
		if offset > 100 {
			break
		}
		offset += limit
	}
	return SnackableFile{}, errors.New("File not found")
}

func (fileService FileService) loadNextFiles(limit int, offset int) ([]SnackableFile, error) {
	return fileService.fileClient.loadFiles(limit, offset)
}

func (fileService FileService) findAllProcessedFiles() []SnackableFile {
	limit := 5
	offset := 0
	var processedFiles []SnackableFile
	for true {
		loadedFiles, err := fileService.loadNextFiles(limit, offset)
		if err != nil {
			log.Println(err)
			return processedFiles
		}

		for _, file := range loadedFiles {
			if file.ProcessingStatus == "FINISHED" {
				processedFiles = append(processedFiles, file)
			}
		}
		if offset > 100 {
			break
		}
		offset += limit
	}

	return processedFiles
}
