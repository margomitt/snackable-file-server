# Snackable files presentation API
Simple API to merge file details from different sources. Server also polls after every 5min file statuses and caches all FINISHED files to system.

## GoLang
version: 1.17

## Build
`go build snackable-file-server`

## Run

### run without params
default port is 8080
run `./snackable-file-server`

### run with custom params(All params need to be provided)
`./snackable-file-server {port} {backendUrl} {cachePath}`

example `./snackable-file-server 8081 http://interview-api.snackable.ai/api/file /var/tmp`