package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"time"
)

type SnackableFileServer struct {
	fileService  IFileService
	cacheService ICacheService
}

type ServerErrorResponse struct {
	StatusCode int    `json:"statusCode"`
	Message    string `json:"message"`
}

func main() {
	port := "8080"
	backendUrl := "http://interview-api.snackable.ai/api/file"
	cachePath := "/var/tmp"

	if len(os.Args) > 3 {
		port = os.Args[1]
		backendUrl = os.Args[2]
		cachePath = os.Args[3]

		log.Printf("Initialized custom params: port: %s, backendUrl: %s, cachePath: %s", port, backendUrl, cachePath)
	}

	server := SnackableFileServer{
		fileService:  FileService{fileClient: SnackableFileClient{url: backendUrl}},
		cacheService: CacheService{Path: cachePath},
	}

	go server.pollAndSaveProcessedFiles()

	http.HandleFunc("/", server.fileWithMetadata)
	log.Println("Starting server at port " + port)
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		log.Fatal(err)
	}
}

func (server SnackableFileServer) fileWithMetadata(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		server.httpErrorResponse(w, ServerErrorResponse{
			StatusCode: http.StatusNotFound,
			Message:    "Endpoint not found",
		})
		return
	}

	if r.Method != "GET" {
		server.httpErrorResponse(w, ServerErrorResponse{
			StatusCode: http.StatusMethodNotAllowed,
			Message:    "Method not allowed",
		})
		return
	}

	fileIdList, ok := r.URL.Query()["fileId"]
	if !ok || len(fileIdList) == 0 {
		server.httpErrorResponse(w, ServerErrorResponse{
			StatusCode: http.StatusBadRequest,
			Message:    "Bad request",
		})
		return
	}

	fileId := fileIdList[0]
	fileWithMetadata, err := server.loadFileWithMetadata(fileId)
	if err != nil {
		server.httpErrorResponse(w, ServerErrorResponse{
			StatusCode: http.StatusInternalServerError,
			Message:    "File data loading failed",
		})
		return
	}

	go server.cacheService.save(fileId, fileWithMetadata)
	server.httpResponse(w, fileWithMetadata, http.StatusOK)
}

func (server SnackableFileServer) loadFileWithMetadata(fileId string) (SnackableFileWithMetadata, error) {
	snackableFileWithMetadata := SnackableFileWithMetadata{}
	err := server.cacheService.get(fileId, &snackableFileWithMetadata) //TODO do we need to invalidate cache ?
	if err == nil {
		return snackableFileWithMetadata, nil
	}

	snackableFile, err := server.fileService.findSnackableFile(fileId)
	if err != nil {
		return SnackableFileWithMetadata{}, err
	}

	return server.fileService.loadFileWithMetadata(snackableFile)
}

func (server SnackableFileServer) pollAndSaveProcessedFiles() {
	for range time.Tick(time.Minute * 5) {
		processedFiles := server.fileService.findAllProcessedFiles()
		for _, processedFile := range processedFiles { //TODO should we update existing ?
			if !server.cacheService.exists(processedFile.FileId) {
				snackableFileWithMetadata, err := server.fileService.loadFileWithMetadata(processedFile)
				if err != nil {
					log.Println(err)
					continue
				}
				server.cacheService.save(processedFile.FileId, snackableFileWithMetadata)
				log.Println("Cached file: " + processedFile.FileId)
			}
		}
	}
}

func (server SnackableFileServer) httpResponse(w http.ResponseWriter, response interface{}, status int) {
	responseJson, err := json.Marshal(response)
	if err != nil {
		status = http.StatusInternalServerError
		responseJson = []byte("Internal server error")
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	if _, e := w.Write(responseJson); e != nil {
		log.Println(e)
	}
}

func (server SnackableFileServer) httpErrorResponse(w http.ResponseWriter, errorResponse ServerErrorResponse) {
	server.httpResponse(w, errorResponse, errorResponse.StatusCode)
}
