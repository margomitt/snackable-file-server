package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

type SnackableFile struct {
	FileId           string `json:"fileId"`
	ProcessingStatus string `json:"processingStatus"`
}

type SnackableFileDetails struct {
	FileName         string `json:"fileName"`
	Mp3Path          string `json:"mp3Path"`
	OriginalFilepath string `json:"originalFilepath"`
	SeriesTitle      string `json:"seriesTitle"`
}

type SnackableFileSegment struct {
	Id        int32  `json:"fileSegmentId"`
	Text      string `json:"segmentText"`
	StartTime int32  `json:"startTime"`
	EndTime   int32  `json:"endTime"`
}

type IFileClient interface {
	loadFiles(limit int, offset int) ([]SnackableFile, error)
	loadFileDetails(fileId string) (SnackableFileDetails, error)
	loadFileSegments(fileId string) ([]SnackableFileSegment, error)
}

type SnackableFileClient struct {
	IFileClient
	url string
}

func (fileClient SnackableFileClient) loadFiles(limit int, offset int) ([]SnackableFile, error) {
	resp, err := http.Get(fileClient.url + "/all?limit=" + strconv.Itoa(limit) + "&offset=" + strconv.Itoa(offset))
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if err := resp.Body.Close(); err != nil {
		log.Println(err)
	}

	var files []SnackableFile
	if err := json.Unmarshal(body, &files); err != nil {
		return nil, err
	}

	return files, nil
}

func (fileClient SnackableFileClient) loadFileDetails(fileId string) (SnackableFileDetails, error) {
	resp, err := http.Get(fileClient.url + "/details/" + fileId)
	if err != nil {
		return SnackableFileDetails{}, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return SnackableFileDetails{}, err
	}
	if err := resp.Body.Close(); err != nil {
		log.Println(err)
	}

	var snackableFileDetails SnackableFileDetails
	if err := json.Unmarshal(body, &snackableFileDetails); err != nil {
		return SnackableFileDetails{}, err
	}

	return snackableFileDetails, nil
}

func (fileClient SnackableFileClient) loadFileSegments(fileId string) ([]SnackableFileSegment, error) {
	resp, err := http.Get(fileClient.url + "/segments/" + fileId)
	if err != nil {
		return []SnackableFileSegment{}, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []SnackableFileSegment{}, err
	}
	if err := resp.Body.Close(); err != nil {
		log.Println(err)
	}

	var snackableFileSegment []SnackableFileSegment
	if err := json.Unmarshal(body, &snackableFileSegment); err != nil {
		return []SnackableFileSegment{}, err
	}

	return snackableFileSegment, nil
}
