package main

import (
	"reflect"
	"testing"
)

func TestLoadFileWithMetadata_Success(t *testing.T) {
	fileService := FileService{
		fileClient: MockFileClient{},
	}

	fileDetails, err := fileService.loadFileWithMetadata(SnackableFile{FileId: "aa-gg", ProcessingStatus: "FINISHED"})
	expectedFileDetails := SnackableFileWithMetadata{
		FileId:           "aa-gg",
		ProcessingStatus: "FINISHED",
		FileName:         "File Name",
		Mp3Path:          "https://s3com/test.mp3",
		OriginalFilepath: "https://s3com/test.mp3",
		SeriesTitle:      "Test files",
		Segments: []SnackableFileSegment{
			{Id: 1, Text: "Segment 1", StartTime: 100, EndTime: 200},
			{Id: 2, Text: "Segment 2", StartTime: 300, EndTime: 450},
		},
	}

	if err != nil {
		t.Error("No errors should be provided")
	}
	if !reflect.DeepEqual(fileDetails, expectedFileDetails) {
		t.Errorf("got %q, expected %q", fileDetails, expectedFileDetails)
	}
}

func TestLoadFileWithMetadata_Failed_FileNotFound(t *testing.T) {
	fileService := FileService{
		fileClient: MockFileClient{},
	}

	_, err := fileService.findSnackableFile("aa-aa")
	if err == nil {
		t.Error("Error should be provided if file not found in list")
	}

	errorMessage := err.Error()
	expectedMessage := "File not found"
	if errorMessage != expectedMessage {
		t.Errorf("got %q, expected %q", errorMessage, expectedMessage)
	}
}

func TestLoadFileWithMetadata_Failed_FileIsNotFinished(t *testing.T) {
	fileService := FileService{
		fileClient: MockFileClient{},
	}

	_, err := fileService.loadFileWithMetadata(SnackableFile{FileId: "aa-cc", ProcessingStatus: "PROCESSING"})
	if err == nil {
		t.Error("Error should be provided if file status is FAILED")
	}

	errorMessage := err.Error()
	expectedMessage := "File is not in FINISHED status"
	if errorMessage != expectedMessage {
		t.Errorf("got %q, expected %q", errorMessage, expectedMessage)
	}
}

func TestFindAllProcessedFiles(t *testing.T) {
	fileService := FileService{
		fileClient: MockFileClient{},
	}

	processedFiles := fileService.findAllProcessedFiles()

	expectedProcessedFiles := []SnackableFile{
		{FileId: "aa-bb", ProcessingStatus: "FINISHED"},
		{FileId: "aa-gg", ProcessingStatus: "FINISHED"},
	}

	if !reflect.DeepEqual(processedFiles, expectedProcessedFiles) {
		t.Errorf("got %q, expected %q", processedFiles, expectedProcessedFiles)
	}
}

type MockFileClient struct {
	IFileClient
}

func (fileClient MockFileClient) loadFiles(limit int, offset int) ([]SnackableFile, error) {
	if offset > 5 {
		return []SnackableFile{}, nil
	}

	if offset == 5 {
		return []SnackableFile{
			{FileId: "aa-gg", ProcessingStatus: "FINISHED"},
		}, nil
	}

	return []SnackableFile{
		{FileId: "aa-bb", ProcessingStatus: "FINISHED"},
		{FileId: "aa-cc", ProcessingStatus: "FAILED"},
		{FileId: "aa-dd", ProcessingStatus: "PROCESSING"},
		{FileId: "aa-ee", ProcessingStatus: "PROCESSING"},
		{FileId: "aa-ff", ProcessingStatus: "PROCESSING"},
	}, nil
}

func (fileClient MockFileClient) loadFileDetails(fileId string) (SnackableFileDetails, error) {

	return SnackableFileDetails{
		FileName:         "File Name",
		Mp3Path:          "https://s3com/test.mp3",
		OriginalFilepath: "https://s3com/test.mp3",
		SeriesTitle:      "Test files",
	}, nil
}

func (fileClient MockFileClient) loadFileSegments(fileId string) ([]SnackableFileSegment, error) {

	return []SnackableFileSegment{
		{Id: 1, Text: "Segment 1", StartTime: 100, EndTime: 200},
		{Id: 2, Text: "Segment 2", StartTime: 300, EndTime: 450},
	}, nil
}
